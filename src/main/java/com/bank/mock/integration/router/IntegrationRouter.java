package com.bank.mock.integration.router;

import com.bank.mock.integration.service.IntegrationService;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.SagaPropagation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegrationRouter extends RouteBuilder {
    @Autowired
    private IntegrationService integrationService;

    @Override
    public void configure() throws Exception {
        from("jms:queue:{{services.integration.process}}")
                .routeId("process_integration")
                .to("log:DEBUG?showBody=true&showHeaders=true&showProperties=true")
                .saga()
                .propagation(SagaPropagation.MANDATORY)
                .compensation("direct:cancel_process_integration")
                .log(" - body: ${body}")
                .bean(integrationService, "integrate")
                .end();

        from("direct:cancel_process_integration")
                .routeId("cancel_process_integration")
                .log("Integration for #${header.id} has been cancelled");
    }
}
