package com.bank.mock.integration.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReservedLimitResponse {
    private TransferRequest transferRequest;
    private UserLimitResponse userLimitResponse;
}
